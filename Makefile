
conf_dir := ~/.configs

.PHONY: all
all: zsh conf aesgcmcli

zsh:
	sudo pacman -Sy --noconfirm zsh
	sudo chsh -s /bin/zsh student
	zsh -i -c -- '-zplg-scheduler burst || true'

conf:
	test -d $(conf_dir) || git clone --recursive https://github.com/robobenklein/configs.git $(conf_dir)
	$(conf_dir)/install

aesgcmcli:
	git submodule sync --recursive
	git submodule update --init --recursive
	cd aesgcm && $(MAKE) test
